const mongoose = require("mongoose");

const month = [
   "January",
   "February",
   "March",
   "April",
   "May",
   "June",
   "July",
   "August",
   "September",
   "October",
   "November",
   "December",
];

const userSchema = new mongoose.Schema({
   username: {
      type: String,
      required: true,
   },
   gender: {
      type: String,
      required: true,
   },
   password: {
      type: String,
      required: true,
   },
   emotionalStatus: {
      type: String,
      required: true,
   },
   isAdmin: {
      type: Boolean,
      default: false,
   },
   createdOn: {
      type: String,
      default: `${new Date().getFullYear()} ${
         month[new Date().getMonth()]
      } ${new Date().getDate()} `,
   },
});

module.exports = mongoose.model("User", userSchema);
