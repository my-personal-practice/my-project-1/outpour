// modules
const mongoose = require("mongoose");
const month = [
   "January",
   "February",
   "March",
   "April",
   "May",
   "June",
   "July",
   "August",
   "September",
   "October",
   "November",
   "December",
];

const postSchema = new mongoose.Schema({
   userId: {
      type: String,
      required: true,
   },
   username: {
      type: String,
      required: true,
   },
   userGender: {
      type: String,
      required: true,
   },
   userEmotionalStatus: {
      type: String,
      required: true,
   },
   userPost: {
      type: String,
      required: true,
   },
   postComments: {
      type: [
         {
            userId: {
               type: String,
               required: true,
            },
            username: {
               type: String,
               required: true,
            },
            userComment: {
               type: String,
            },
            likes: {
               type: [
                  {
                     username: {
                        type: String,
                     },
                     like: {
                        type: Number,
                        default: 1,
                     },
                  },
               ],
               default: null,
            },
            createdOn: {
               type: String,
               default: `${new Date().getFullYear()} ${
                  month[new Date().getMonth()]
               } ${new Date().getDate()} `,
            },
         },
      ],
      default: null,
   },
   likes: {
      type: [
         {
            username: {
               type: String,
            },
            like: {
               type: Number,
               default: 1,
            },
         },
      ],
      default: null,
   },
   createdOn: {
      type: String,
      default: `${new Date().getFullYear()} ${
         month[new Date().getMonth()]
      } ${new Date().getDate()} `,
   },
});

module.exports = mongoose.model("Post", postSchema);
