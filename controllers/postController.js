// Local modules
const User = require("../models/User");
const Post = require("../models/Post");

// create a post
module.exports.createPost = (data) => {
   // checking if the auth is not empty
   if (data.userData == null) {
      return false;
   } else {
      // finding the user details
      return User.findById(data.userData).then((result) => {
         if (result != null) {
            // creating the post
            let post = new Post({
               userId: result._id,
               username: result.username,
               userGender: result.gender,
               userEmotionalStatus: result.emotionalStatus,
               userPost: data.userPost,
            });
            // saving the post
            return post.save().then((result, error) => {
               if (error) {
                  return false;
               } else {
                  return true;
               }
            });
         } else {
            return false;
         }
      });
   }
};

// retrieve user posts
module.exports.getUserPosts = (data) => {
   // checking if the auth is not empty
   if (data.userData == null) {
      return false;
   } else {
      // finding the posts by userId
      return Post.find({ userId: data.userData }).then((result) => {
         if (result != null) {
            return result;
         } else {
            return false;
         }
      });
   }
};

// retrieve all user posts
module.exports.getAllUserPosts = (data) => {
   // finding the all posts
   return Post.find().then((result) => {
      if (result != null) {
         return result;
      } else {
         return false;
      }
   });
};

// edit post
module.exports.editPost = (data) => {
   // checking if the auth is not empty
   if (data.userData == null) {
      return false;
   } else {
      // finding the posts by userId
      return Post.findById(data.postId).then((result) => {
         if (result != null) {
            // editing the post
            result.userPost = data.userPost;

            // saving the edited post
            return result.save().then((result, error) => {
               if (error) {
                  return false;
               } else {
                  return true;
               }
            });
         } else {
            return false;
         }
      });
   }
};

// delete a post
module.exports.deletePost = (data) => {
   // checking if the auth is not empty
   if (data.userData == null) {
      return false;
   } else {
      // finding the post of the user via id and delete
      return Post.findByIdAndDelete(data.postId).then((result) => {
         if (result != null) {
            return true;
         } else {
            return false;
         }
      });
   }
};

// add comment to a post
module.exports.addComment = async (data) => {
   // checking if the auth is not empty
   if (data.userId == null) {
      return false;
   } else {
      // finding the post via id
      return Post.findById(data.postId).then((result) => {
         // checking if the post is exists
         if (result != null) {
            // creating a comment to a post
            let comment = {
               userId: data.userId,
               username: data.username,
               userComment: data.userComment,
            };
            // pushing the created comment to the post
            result.postComments.push(comment);
            // saving the pushed comment
            return result.save().then((result) => {
               if (result != null) {
                  return true;
               } else {
                  return false;
               }
            });
         } else {
            return false;
         }
      });
   }
};

// liking a post
module.exports.likePost = async (data) => {
   // checking if the auth is not empty
   if (data.userId == null) {
      return false;
   } else {
      // finding the post via post id
      return Post.findById(data.postId).then((post) => {
         // checking if the post is not empty
         if (post != null) {
            // checking the user's info via id
            return User.findById(data.userId).then((user) => {
               // checking if the user is existing
               if (user != null) {
                  // making an object
                  let like = {
                     username: user.username,
                  };

                  // filtering the users who already like the post
                  let filteredLikes = post.likes.filter((like) => {
                     return like.username === user.username;
                  });

                  // checking if the user already likes the post
                  if (filteredLikes.length > 0) {
                     return false;
                  } else {
                     // if the user doesn't yet like the post
                     post.likes.push(like);

                     return post.save().then((save, error) => {
                        if (save) {
                           return true;
                        } else {
                           return false;
                        }
                     });
                  }
               } else {
                  return false;
               }
            });
         } else {
            return false;
         }
      });
   }
};

// liking a post comment
module.exports.likePostComment = async (data) => {
   // checking if the auth is empty
   if (data.userId == null) {
      return false;
   } else {
      // finding the post via id
      return Post.findById(data.postId).then((post) => {
         if (post != null) {
            // checking if the users is existing
            return User.findById(data.userId).then((user) => {
               if (user != null) {
                  let like = {
                     username: user.username,
                  };

                  // finding the comment index location to be like
                  let commentIndex = post.postComments.findIndex((comment) => {
                     return comment.id === data.commentId;
                  });

                  // creating a variable for the data location
                  let commentLikes = post.postComments[commentIndex].likes;
                  // filtering comments with existing liked by the user
                  let isAlreadyLiked = commentLikes.filter((like) => {
                     return like.username === user.username;
                  });
                  // checking if the filtered liked by the user
                  if (isAlreadyLiked.length > 0) {
                     return false;
                  } else {
                     // pushing the like to the comment
                     commentLikes.push(like);

                     // saving the comment like
                     return post.save().then((save, error) => {
                        if (save) {
                           return true;
                        } else {
                           return false;
                        }
                     });
                  }
               }
            });
         } else {
            return false;
         }
      });
   }
};
