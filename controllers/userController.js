// Modules
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Local modules
const User = require("../models/User");
const auth = require("../auth");

//Register a user
module.exports.registerUser = (data) => {
   let user = new User({
      username: data.username,
      gender: data.gender,
      password: bcrypt.hashSync(data.password, 10),
      emotionalStatus: data.emotionalStatus,
   });
   // Looking if the username already exists
   return User.findOne({ username: user.username }).then((result) => {
      if (result != null) {
         return false;
      } else {
         return user.save().then((result, error) => {
            if (error) {
               return false;
            } else {
               return true;
            }
         });
      }
   });
};

// loginUser
module.exports.loginUser = (data) => {
   return User.findOne({ username: data.username }).then((result) => {
      if (result != null) {
         const isPasswordCorrect = bcrypt.compareSync(
            data.password,
            result.password
         );
         if (isPasswordCorrect) {
            return { access: auth.createAccessToken(result) };
         } else {
            return false;
         }
      } else {
         return false;
      }
   });
};

// retrieve all users only admin
module.exports.getAllUsers = async (data, error) => {
   // check if the access token is admin
   if (data.isAdmin) {
      // finding all users
      return User.find(data.reqBody).then((result) => {
         return result;
      });
   } else {
      return false;
   }
};

// retrieve single users
module.exports.getSingleUser = async (data, error) => {
   // check if the access token is admin
   if (data.isAdmin) {
      // finding all users
      return User.findById(data.userId).then((result) => {
         if (result != null) {
            return result;
         } else {
            return false;
         }
      });
   } else {
      return false;
   }
};
