//Modules
const express = require("express");

// local modules
const auth = require("../auth.js");
const postController = require("../controllers/postController.js");

// initialize router
const router = express.Router();
// create post
router.post("/createPost", auth.verify, (req, res) => {
   const data = {
      userData: auth.decode(req.headers.authorization).id,
      userPost: req.body.post,
   };

   postController.createPost(data).then((resultFromTheController) => {
      res.send(resultFromTheController);
   });
});

// retrieve user posts
router.get("/userPost/", auth.verify, (req, res) => {
   const data = {
      userData: auth.decode(req.headers.authorization).id,
   };

   postController.getUserPosts(data).then((resultFromTheController) => {
      res.send(resultFromTheController);
   });
});

// retrieve user posts
router.get("/", (req, res) => {
   const user = req.body;

   postController.getAllUserPosts(user).then((resultFromTheController) => {
      res.send(resultFromTheController);
   });
});

// edit post
router.put("/editPost/:postId", auth.verify, (req, res) => {
   const data = {
      userData: auth.decode(req.headers.authorization).id,
      postId: req.params.postId,
      userPost: req.body.userPost,
   };

   postController.editPost(data).then((resultFromTheController) => {
      res.send(resultFromTheController);
   });
});
// delete post
router.delete("/deletePost/:postId", auth.verify, (req, res) => {
   const data = {
      userData: auth.decode(req.headers.authorization).id,
      postId: req.params.postId,
   };

   postController.deletePost(data).then((resultFromTheController) => {
      res.send(resultFromTheController);
   });
});

// add comment to a post
router.post("/addComment/:postId", auth.verify, (req, res) => {
   const data = {
      userId: auth.decode(req.headers.authorization).id,
      username: auth.decode(req.headers.authorization).username,
      postId: req.params.postId,
      userComment: req.body.userComment,
   };

   postController.addComment(data).then((resultFromTheController) => {
      res.send(resultFromTheController);
   });
});

// like post
router.put("/likePost/:postId", auth.verify, (req, res) => {
   const data = {
      userId: auth.decode(req.headers.authorization).id,
      postId: req.params.postId,
   };

   postController.likePost(data).then((resultFromTheController) => {
      res.send(resultFromTheController);
   });
});

// like a post comment
router.put("/likePostComment/:postId", auth.verify, (req, res) => {
   const data = {
      userId: auth.decode(req.headers.authorization).id,
      postId: req.params.postId,
      commentId: req.body.commentId,
   };

   postController.likePostComment(data).then((resultFromTheController) => {
      res.send(resultFromTheController);
   });
});

// exports all routers
module.exports = router;
