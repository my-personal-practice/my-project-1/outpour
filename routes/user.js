// Modules
const express = require("express");

// Local Modules
const auth = require("../auth.js");
const userController = require("../controllers/userController.js");

// initialize router
const router = express.Router();

// register user
router.post("/register", (req, res) => {
   const data = req.body;

   userController.registerUser(data).then((resultFromTheController) => {
      res.send(resultFromTheController);
   });
});

// login user
router.post("/login", (req, res) => {
   userController.loginUser(req.body).then((resultFromTheController) => {
      res.send(resultFromTheController);
   });
});

// retrieve all users
router.get("/", auth.verify, (req, res) => {
   const data = {
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      reqBody: req.body,
   };

   userController.getAllUsers(data).then((resultFromTheController) => {
      res.send(resultFromTheController);
   });
});

// retrieve a single user
router.get("/userDetails/:userId", auth.verify, (req, res) => {
   const data = {
      isAdmin: auth.decode(req.headers.authorization).isAdmin,
      userId: req.params.userId,
   };

   userController.getSingleUser(data).then((resultFromTheController) => {
      res.send(resultFromTheController);
   });
});

// export router
module.exports = router;
