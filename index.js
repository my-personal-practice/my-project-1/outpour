// Importing modules
const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/user.js");
const postRoutes = require("./routes/post.js");
const cors = require("cors");

const app = express();

// connecting to database
mongoose.connect(
   "mongodb+srv://nanrodriguez:monabamy6970@cluster0.phps5an.mongodb.net/Outpour?retryWrites=true&w=majority",
   {
      useNewUrlParser: true,
      useUnifiedTopology: true,
   }
);

// setting notifications if the database is connected to the server
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("We are now connected to the database"));

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/users", userRoutes);
app.use("/posts", postRoutes);

// Server listening
app.listen(process.env.PORT || 4000, () => {
   console.log(`API is now connected on port: ${process.env.PORT}`);
});
